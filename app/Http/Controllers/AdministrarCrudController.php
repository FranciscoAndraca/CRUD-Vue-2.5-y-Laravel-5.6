<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class AdministrarCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return response()->json($posts);
    }

    public function posts()
    {
      	return view('crud.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $post  = new Post;
      $post->nombre = $request->titulo;
      $post->descripcion = $request->comentario;

      if ($post->save()) {
        return response()->json($post);
      } else {
        return response()->json( [ 'post' => 'NO' ] );
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $actualizar_post = Post::find($id);
        $actualizar_post->nombre = $request->titulo;
        $actualizar_post->descripcion =$request->comentario;
        $actualizar_post->update();

        return response()->json($actualizar_post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Post::find($id)->delete();
      return response()->json(['ok']);
    }
}
